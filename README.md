# Overview

The goal is to build the backend for a simple e-wallet. The functionality is to be exposed as REST APIs as per the documentation provided. The wallet can be implemented in any language of your choice. The complete source code directory should be submitted as a tar/zip and a detailed README with instructions on building and running the service should be included.

Following are the main operations expected from the wallet:

* Register new user
* Read balance
* Balance top-up
* Money transfer between wallets
* List top N transactions by value per user
* List overall top N transacting users by value

# Prerequisites

* Go1.18

# Getting Started

Go to `config` directory,

* `$ cp sample.app.toml app.toml` to copy sample app toml (containing envs/secrets).

Run these following commands (visit `makefile`):

* `$ make e2e` to run E2E testing.
* `$ make api` to run restful server.
* `$ make build` to build the executable binary.

# Project Structures

* `config` contains toml files for envs/secrets.
* `e2e` contains E2E tests.
* `entity` contains many `struct`, `error`, `var`, `const` that can be used in multiple code layers.
* `event` contains general purpose observer pattern.
* `external` contains external dependency.
* `infra` contains infrastructure builder code.
* `repo` contains data storage code.
* `restful` contains restful handlers, custom restful middlewares and restful route.
* `service` contains reusable service codes. Middle layer between handlers (`restful`, `grpc`, etc) and `repo`.
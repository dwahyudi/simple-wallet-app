package event

import (
	"simple-wallet/repo/balancestatrepo"
	"sync"
)

var (
	BalanceEventOnce sync.Once
	balanceEventSvc  BalanceEvent
)

func InjectNewBalanceService() BalanceEvent {
	BalanceEventOnce.Do(func() {
		balanceStatRepo := balancestatrepo.InjectNewBalanceRepo()

		balanceEventSvc = &balanceEvent{
			transferObservers: []TransferObserver{
				balanceStatRepo,
			},
		}
	})

	return balanceEventSvc
}

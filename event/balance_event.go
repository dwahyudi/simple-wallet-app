package event

import (
	"context"
	"simple-wallet/entity"
)

type (
	TransferObserver interface {
		Update(ctx context.Context, transferEvent entity.TransferEvent) error
	}

	BalanceEvent interface {
		TransferNotify(ctx context.Context, transferEvent entity.TransferEvent)
	}

	balanceEvent struct {
		transferObservers []TransferObserver
	}
)

func (b *balanceEvent) TransferNotify(
	ctx context.Context, transferEvent entity.TransferEvent,
) {
	for _, eachObserver := range b.transferObservers {
		eachObserver.Update(ctx, transferEvent)
	}
}

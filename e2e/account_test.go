package e2e_test

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestV1RegisterUser(t *testing.T) {
	requestBody := `
	{
		"username": "budi"
	}`

	resp, err := http.Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"), "application/json", strings.NewReader(requestBody))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	assert.Equal(t, http.StatusCreated, resp.StatusCode)

	resp, err = http.Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"), "application/json", strings.NewReader(requestBody))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusConflict, resp.StatusCode)
	assert.Equal(t, "Username already exists", string(b))
}

func TestV1RegisterUser_BlankUsername(t *testing.T) {
	requestBody := `
	{
		"username": ""
	}`

	resp, err := http.Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"), "application/json", strings.NewReader(requestBody))
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode)
}

package e2e_test

import (
	"log"
	"os"
	"simple-wallet/external"
	"simple-wallet/infra"
	"simple-wallet/restful"
	"testing"
	"time"
)

const (
	BaseURL   = "http://127.0.0.1:7777"
	V1BaseURL = "http://127.0.0.1:7777/v1"
)

func TestMain(m *testing.M) {
	log.Print("Initiating End to End tests...")

	log.Print("Setting up End to End tests...")
	prepared := make(chan (bool))
	go func() {
		log.Println("Setting up Web Server")
		infraInject := infra.NewInfra("../config/e2e.toml", external.InjectNewExternalService("../config/e2e.toml", nil))

		restful.InjectNewServer(infraInject).Run(prepared)
	}()

	<-prepared
	log.Print("Setting up Complete...")

	time.Sleep(2 * time.Duration(time.Second))

	log.Print("Running tests...")
	code := m.Run()

	os.Exit(code)
}

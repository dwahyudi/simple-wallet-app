package e2e_test

import (
	"fmt"
	"log"
	"net/http"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
)

func TestV1Topup(t *testing.T) {
	cl := resty.New()

	requestBody := `
	{
		"username": "jono"
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	auth := string(resp.Body())

	requestBody = `
	{
		"amount": 17000
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auth).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auth).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":17000}`, string(resp.Body()))

	requestBody = `
	{
		"amount": 2500
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auth).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auth).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":19500}`, string(resp.Body()))
}

func TestV1Topup_TooBigAmount(t *testing.T) {
	cl := resty.New()

	requestBody := `
	{
		"username": "david"
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	auth := string(resp.Body())

	requestBody = `
	{
		"amount": 10000001
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auth).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
	assert.Equal(t, `Topup amount too big`, string(resp.Body()))
}

func TestV1Topup_NonPositiveAmount(t *testing.T) {
	cl := resty.New()

	requestBody := `
	{
		"username": "maryono"
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	auth := string(resp.Body())

	requestBody = `
	{
		"amount": -1
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auth).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
	assert.Equal(t, `Invalid topup amount`, string(resp.Body()))
}

func TestV1Transfer(t *testing.T) {
	cl := resty.New()

	// create charles username
	requestBody := `
	{
		"username": "charles"
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authCharles := string(resp.Body())

	// create thomas username
	requestBody = `
	{
		"username": "thomas"
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authThomas := string(resp.Body())

	// charles topup 11000
	requestBody = `
	{
		"amount": 11000
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authCharles).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	// charles check balance
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authCharles).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":11000}`, string(resp.Body()))

	// charles transfer to thomas 5333.8
	requestBody = `
	{
		"amount": 5333.8,
		"to_username": "thomas"
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authCharles).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	// charles check balance again
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authCharles).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":5666.2}`, string(resp.Body()))

	// thomas check balance
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authThomas).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":5333.8}`, string(resp.Body()))
}

func TestV1Transfer_NonPositiveTransfer(t *testing.T) {
	cl := resty.New()

	// create arif username
	requestBody := `
	{
		"username": "arif"
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authArif := string(resp.Body())

	// create mickey username
	requestBody = `
	{
		"username": "mickey"
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authMickey := string(resp.Body())

	// arif topup 9000
	requestBody = `
	{
		"amount": 9000
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authArif).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	// arif transfer to mickey -1, should not be allowed
	requestBody = `
		{
			"amount": -1,
			"to_username": "mickey"
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authArif).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
	assert.Equal(t, `Transfer should be more than 0`, string(resp.Body()))

	// arif balance shouldn't change
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authArif).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":9000}`, string(resp.Body()))

	// mickey balance shouldn't change
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authMickey).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":0}`, string(resp.Body()))
}

func TestV1Transfer_DestinationUsernameNotFound(t *testing.T) {
	cl := resty.New()

	// create takahashi username
	requestBody := `
		{
			"username": "takahashi"
		}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authTakahashi := string(resp.Body())

	// takahashi topup 9000
	requestBody = `
		{
			"amount": 9000
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authTakahashi).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	// takahashi transfer 2000 to non-exist username
	requestBody = `
		{
			"amount": 2000,
			"to_username": "akira"
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authTakahashi).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
	assert.Equal(t, `Destination user not found`, string(resp.Body()))

	// takahashi balance shouldn't change
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authTakahashi).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":9000}`, string(resp.Body()))
}

func TestV1Transfer_BlankDestinationUsername(t *testing.T) {
	cl := resty.New()

	// create joey username
	requestBody := `
		{
			"username": "joey"
		}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authJoey := string(resp.Body())

	// joey topup 9000
	requestBody = `
		{
			"amount": 9000
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authJoey).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	// joey transfer 2000 to non-exist username
	requestBody = `
		{
			"amount": 2000,
			"to_username": ""
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authJoey).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
	assert.Equal(t, `Blank destination username`, string(resp.Body()))

	// joey balance shouldn't change
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authJoey).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":9000}`, string(resp.Body()))
}

func TestV1Transfer_InsufficientBalance(t *testing.T) {
	cl := resty.New()

	// create alexander username
	requestBody := `
	{
		"username": "alexander"
	}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authAlexander := string(resp.Body())

	// create khalid username
	requestBody = `
	{
		"username": "khalid"
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authKhalid := string(resp.Body())

	// alexander topup 9000
	requestBody = `
	{
		"amount": 9000
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authAlexander).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusNoContent, resp.StatusCode())

	// alexander transfer to khalid 12000, should not be allowed
	requestBody = `
		{
			"amount": 12000,
			"to_username": "khalid"
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authAlexander).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusUnprocessableEntity, resp.StatusCode())
	assert.Equal(t, `Insufficient balance`, string(resp.Body()))

	// alexander balance shouldn't change
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authAlexander).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":9000}`, string(resp.Body()))

	// khalid balance shouldn't change
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authKhalid).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "balance_read"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `{"balance":0}`, string(resp.Body()))
}

func TestV1TopUsers_NoData(t *testing.T) {
	cl := resty.New()

	// reset stats data
	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", "k7prxjg2metkzvcajs6ntjwfu4ngkyuj").
		Get(fmt.Sprintf("%s/%s", BaseURL, "reset_stat"))
	if err != nil {
		log.Fatalln(err)
	}

	// create sudrajat username
	requestBody := `
		{
			"username": "sudrajat"
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authSudrajat := string(resp.Body())

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authSudrajat).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_users"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[]`, string(resp.Body()))
}

func TestV1TopUsers_LessThan10(t *testing.T) {
	cl := resty.New()

	// reset stats data
	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", "k7prxjg2metkzvcajs6ntjwfu4ngkyuj").
		Get(fmt.Sprintf("%s/%s", BaseURL, "reset_stat"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	// create a test clothing store account to receive transfer
	requestBody := `
	{
		"username": "tokobaju_oke"
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authTokoBaju := string(resp.Body())

	usernames := []string{
		"abella", "barjo", "cecil", "dekisugi", "enrico", "farooq",
	}

	transfersCount := map[string]int{
		"abella":   20,
		"barjo":    6,
		"cecil":    33,
		"dekisugi": 0,
		"enrico":   9,
		"farooq":   44,
	}

	auths := make(map[string]string)

	// create bulk usernames and topups and transfers (to tokobaju_oke)
	for _, eachUsername := range usernames {
		// create username
		requestBody := `
		{
			"username": "%s"
		}`

		resp, err := cl.R().
			SetHeader("content-type", "application/json").
			SetBody(fmt.Sprintf(requestBody, eachUsername)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusCreated, resp.StatusCode())
		auth := string(resp.Body())
		auths[eachUsername] = auth

		// create topup
		requestBody = `
		{
			"amount": 2000000
		}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", auth).
			SetBody(requestBody).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())

		// create transfers
		transactionsCount := transfersCount[eachUsername]
		for i := 0; i < transactionsCount; i++ {
			requestBody = `
			{
				"amount": 10000,
				"to_username": "tokobaju_oke"
			}`

			resp, err = cl.R().
				SetHeader("content-type", "application/json").
				SetHeader("Authorization", auth).
				SetBody(requestBody).
				Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
			if err != nil {
				log.Fatalln(err)
			}

			assert.Equal(t, http.StatusNoContent, resp.StatusCode())
		}
	}

	// check if transferred correctly
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authTokoBaju).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_users"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[{"username":"farooq","transacted_value":440000},{"username":"cecil","transacted_value":330000},{"username":"abella","transacted_value":200000},{"username":"enrico","transacted_value":90000},{"username":"barjo","transacted_value":60000}]`, string(resp.Body()))
}

func TestV1TopUsers(t *testing.T) {
	cl := resty.New()

	// reset stats data
	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", "k7prxjg2metkzvcajs6ntjwfu4ngkyuj").
		Get(fmt.Sprintf("%s/%s", BaseURL, "reset_stat"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	// create a test clothing store account to receive transfer
	requestBody := `
		{
			"username": "tokobaju_oke3"
		}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authTokoBaju := string(resp.Body())

	usernames := []string{
		"arum", "brody", "coco", "darius",
		"erlangga", "felix", "ghofar", "haruka",
		"indri", "jackson", "kadek", "larson",
	}

	auths := make(map[string]string)

	transferAmounts := []struct {
		source string
		amount float64
	}{
		{source: "arum", amount: 25000},
		{source: "erlangga", amount: 25000},
		{source: "kadek", amount: 1},
		{source: "brody", amount: 12000},
		{source: "arum", amount: 4000.12},
		{source: "ghofar", amount: 4000},
		{source: "coco", amount: 90000},
		{source: "brody", amount: 40000},
		{source: "indri", amount: 1250},
		{source: "indri", amount: 2500},
		{source: "indri", amount: 2500},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "larson", amount: 200},
		{source: "jackson", amount: 3000},
		{source: "jackson", amount: 3000},
		{source: "jackson", amount: 3000},
		{source: "jackson", amount: 3000},
		{source: "jackson", amount: 3000},
		{source: "haruka", amount: 800000},
		{source: "kadek", amount: 500000.1},
		{source: "arum", amount: 330000},
		{source: "felix", amount: 52000},
	}

	// create bulk usernames
	for _, eachUsername := range usernames {
		// create username
		requestBody := `
		{
			"username": "%s"
		}`

		resp, err := cl.R().
			SetHeader("content-type", "application/json").
			SetBody(fmt.Sprintf(requestBody, eachUsername)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusCreated, resp.StatusCode())
		auth := string(resp.Body())
		auths[eachUsername] = auth

		// create topup
		requestBody = `
		{
			"amount": 2000000
		}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", auth).
			SetBody(requestBody).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())
	}

	for _, transfer := range transferAmounts {
		requestBody = `
			{
				"amount": %v,
				"to_username": "tokobaju_oke3"
			}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", auths[transfer.source]).
			SetBody(fmt.Sprintf(requestBody, transfer.amount)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())
	}

	// check if transferred correctly
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authTokoBaju).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_users"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	/*
		haruka: 800000 only
		kadek: 1 + 500000.1 = 500000.1
		arum: 25000 + 4000 + 330000 = 359000
		coco: 90000 only
		felix: 52000 only
		brody: 12000 + 40000 = 52000
		erlangga: 25000 only
		jackson: 3000 * 5 = 15000
		indri: 1250 + 2500 + 2500 = 6250
		ghofar: 4000 only
		larson: 200 * 10 = 2000 (not top 10) not included in list
	*/
	assert.Equal(t, `[{"username":"haruka","transacted_value":800000},{"username":"kadek","transacted_value":500001.1},{"username":"arum","transacted_value":359000.12},{"username":"coco","transacted_value":90000},{"username":"felix","transacted_value":52000},{"username":"brody","transacted_value":52000},{"username":"erlangga","transacted_value":25000},{"username":"jackson","transacted_value":15000},{"username":"indri","transacted_value":6250},{"username":"ghofar","transacted_value":4000}]`, string(resp.Body()))
}

func TestV1TopUsers_MoreThan10(t *testing.T) {
	cl := resty.New()

	// reset stats data
	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", "k7prxjg2metkzvcajs6ntjwfu4ngkyuj").
		Get(fmt.Sprintf("%s/%s", BaseURL, "reset_stat"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())

	// create a test clothing store account to receive transfer
	requestBody := `
	{
		"username": "tokobaju_oke2"
	}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authTokoBaju := string(resp.Body())

	usernames := []string{
		"andreas", "benny", "caca", "danielson", "endang", "fuji", "gombloh", "hansel", "ivanovic", "jeremy", "krismanto", "larry", "mahmoud",
	}

	transfersCount := map[string]int{
		"andreas":   4,
		"benny":     62,
		"caca":      12,
		"danielson": 4,
		"endang":    9,
		"fuji":      44,
		"gombloh":   0,
		"hansel":    1,
		"ivanovic":  43,
		"jeremy":    23,
		"krismanto": 12,
		"larry":     57,
		"mahmoud":   39,
	}

	auths := make(map[string]string)

	// create bulk usernames and topups and transfers (to tokobaju_oke)
	for _, eachUsername := range usernames {
		// create username
		requestBody := `
		{
			"username": "%s"
		}`

		resp, err := cl.R().
			SetHeader("content-type", "application/json").
			SetBody(fmt.Sprintf(requestBody, eachUsername)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusCreated, resp.StatusCode())
		auth := string(resp.Body())
		auths[eachUsername] = auth

		// create topup
		requestBody = `
		{
			"amount": 2000000
		}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", auth).
			SetBody(requestBody).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())

		// create transfers
		transactionsCount := transfersCount[eachUsername]
		for i := 0; i < transactionsCount; i++ {
			requestBody = `
			{
				"amount": 10000,
				"to_username": "tokobaju_oke2"
			}`

			resp, err = cl.R().
				SetHeader("content-type", "application/json").
				SetHeader("Authorization", auth).
				SetBody(requestBody).
				Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
			if err != nil {
				log.Fatalln(err)
			}

			assert.Equal(t, http.StatusNoContent, resp.StatusCode())
		}
	}

	// check if transferred correctly
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authTokoBaju).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_users"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[{"username":"benny","transacted_value":620000},{"username":"larry","transacted_value":570000},{"username":"fuji","transacted_value":440000},{"username":"ivanovic","transacted_value":430000},{"username":"mahmoud","transacted_value":390000},{"username":"jeremy","transacted_value":230000},{"username":"krismanto","transacted_value":120000},{"username":"caca","transacted_value":120000},{"username":"endang","transacted_value":90000},{"username":"danielson","transacted_value":40000}]`, string(resp.Body()))
}

func TestV1TopTransfers_NoData(t *testing.T) {
	cl := resty.New()

	// create perry username
	requestBody := `
		{
			"username": "perry"
		}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authPerry := string(resp.Body())

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authPerry).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_transactions_per_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[]`, string(resp.Body()))
}

func TestV1TopTransfers(t *testing.T) {
	cl := resty.New()

	// create william username
	requestBody := `
		{
			"username": "william"
		}`

	resp, err := cl.R().
		SetHeader("content-type", "application/json").
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode())
	authWilliam := string(resp.Body())

	// create topup for william
	requestBody = `
			{
				"amount": 2000000
			}`

	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authWilliam).
		SetBody(requestBody).
		Post(fmt.Sprintf("%s/%s", V1BaseURL, "balance_topup"))
	if err != nil {
		log.Fatalln(err)
	}

	usernames := []string{
		"astuti", "bob", "chuck", "diding", "emira", "fachrul",
	}

	transfersAmount := map[string]float64{
		"astuti":  2000,
		"bob":     19000,
		"chuck":   9000,
		"diding":  8777,
		"emira":   250000,
		"fachrul": 121000,
	}

	transferAgain := map[string]float64{
		"astuti": 32500,
		"bob":    1210000,
		"diding": 2777,
		"emira":  55000,
	}

	transferBacks := map[string]float64{
		"bob":   11000,
		"emira": 9000,
	}

	auths := make(map[string]string)

	// create bulk usernames (destinations)
	for _, eachUsername := range usernames {
		// create username
		requestBody := `
			{
				"username": "%s"
			}`

		resp, err := cl.R().
			SetHeader("content-type", "application/json").
			SetBody(fmt.Sprintf(requestBody, eachUsername)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "create_user"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusCreated, resp.StatusCode())
		auth := string(resp.Body())
		auths[eachUsername] = auth
	}

	// create transfers (from william to those destinations)
	for destinationUsername, amount := range transfersAmount {
		requestBody = `
				{
					"amount": %v,
					"to_username": "%v"
				}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", authWilliam).
			SetBody(fmt.Sprintf(requestBody, amount, destinationUsername)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())
	}

	for sourceUsername, amount := range transferBacks {
		requestBody = `
		{
			"amount": %v,
			"to_username": "%v"
		}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", auths[sourceUsername]).
			SetBody(fmt.Sprintf(requestBody, amount, "william")).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())
	}

	// check if  transferred correctly for william
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authWilliam).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_transactions_per_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[{"username":"emira","amount":-250000},{"username":"fachrul","amount":-121000},{"username":"bob","amount":-19000},{"username":"bob","amount":11000},{"username":"emira","amount":9000},{"username":"chuck","amount":-9000},{"username":"diding","amount":-8777},{"username":"astuti","amount":-2000}]`, string(resp.Body()))

	// check if  transferred correctly for emira
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", auths["emira"]).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_transactions_per_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[{"username":"william","amount":250000},{"username":"william","amount":-9000}]`, string(resp.Body()))

	// create transfers (from william to those destinations) again
	for destinationUsername, amount := range transferAgain {
		requestBody = `
					{
						"amount": %v,
						"to_username": "%v"
					}`

		resp, err = cl.R().
			SetHeader("content-type", "application/json").
			SetHeader("Authorization", authWilliam).
			SetBody(fmt.Sprintf(requestBody, amount, destinationUsername)).
			Post(fmt.Sprintf("%s/%s", V1BaseURL, "transfer"))
		if err != nil {
			log.Fatalln(err)
		}

		assert.Equal(t, http.StatusNoContent, resp.StatusCode())
	}

	// check if  transferred correctly for william again
	resp, err = cl.R().
		SetHeader("content-type", "application/json").
		SetHeader("Authorization", authWilliam).
		Get(fmt.Sprintf("%s/%s", V1BaseURL, "top_transactions_per_user"))
	if err != nil {
		log.Fatalln(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode())
	assert.Equal(t, `[{"username":"bob","amount":-1210000},{"username":"emira","amount":-250000},{"username":"fachrul","amount":-121000},{"username":"emira","amount":-55000},{"username":"astuti","amount":-32500},{"username":"bob","amount":-19000},{"username":"bob","amount":11000},{"username":"emira","amount":9000},{"username":"chuck","amount":-9000},{"username":"diding","amount":-8777}]`, string(resp.Body()))

}

package mdw

import (
	"simple-wallet/infra"
	"sync"
)

var (
	mdwOnce sync.Once
	mdwSvc  CustomMiddleware
)

func InjectNewMiddleware(infra infra.Infra) CustomMiddleware {
	mdwOnce.Do(func() {
		mdwSvc = &mdw{
			extCommonSvc:  infra.External().CommonSvc(),
			internalToken: infra.Config().Internal.InternalSecret,
		}
	})
	return mdwSvc
}

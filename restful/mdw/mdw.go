package mdw

import (
	"context"
	"net/http"
	"simple-wallet/entity"
	"simple-wallet/external/extcommon"
	"simple-wallet/restful/restfulcommon"
)

type (
	CustomMiddleware interface {
		AuthenticateToken(next http.Handler) http.Handler
		AuthenticateInternalToken(next http.Handler) http.Handler
	}

	mdw struct {
		internalToken string
		extCommonSvc  extcommon.CommonService
	}
)

func (mdw *mdw) AuthenticateToken(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		authToken := r.Header.Get("Authorization")
		if authToken == "" {
			restfulcommon.SetResponse(w, http.StatusUnauthorized, "unauthorized")
			return
		}

		username, err := mdw.extCommonSvc.JWTValidateLogin(authToken)
		if err != nil {
			restfulcommon.SetResponse(w, http.StatusInternalServerError, "internal server error")
			return
		}

		ctx = context.WithValue(ctx, entity.UsernameKey, username)

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

func (mdw *mdw) AuthenticateInternalToken(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		authToken := r.Header.Get("Authorization")
		if authToken != mdw.internalToken {
			restfulcommon.SetResponse(w, http.StatusUnauthorized, "unauthorized")
			return
		}

		next.ServeHTTP(w, r.WithContext(r.Context()))
	}

	return http.HandlerFunc(fn)
}

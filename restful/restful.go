package restful

import (
	"net/http"
	"simple-wallet/infra"
	"simple-wallet/restful/mdw"
	"simple-wallet/restful/v1/accountrestful"
	"simple-wallet/restful/v1/balancedomainrestful"
	"simple-wallet/restful/v1/balancerestful"
	"simple-wallet/restful/v1/balancestatrestful"
	"simple-wallet/service/accountservice"
	"simple-wallet/service/balanceservice"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
)

type (
	RestfulServer interface {
		Run(prepared chan (bool))
	}

	restfulServer struct {
		infra infra.Infra
	}
)

func InjectNewServer(infra infra.Infra) RestfulServer {
	return &restfulServer{
		infra: infra,
	}
}

func (rs *restfulServer) Run(prepared chan (bool)) {
	var (
		r     = chi.NewRouter()
		infra = rs.infra
		mdw   = mdw.InjectNewMiddleware(infra)

		balanceService = balanceservice.InjectNewBalanceService()
		accountService = accountservice.InjectNewAccountService(infra)

		accountRestful       = accountrestful.InjectNewRestfulVar(infra, balanceService, accountService)
		balanceDomainRestful = balancedomainrestful.InjectNewBalanceDomain(infra)
		balanceRestful       = balancerestful.InjectNewBalanceRestful()
		balanceStatRestful   = balancestatrestful.InjectNewBalanceStatRestful()
	)

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.StripSlashes)

	r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("pong"))
	})

	r.Route("/v1", func(v1Route chi.Router) {
		v1Route.Post("/create_user", accountRestful.RegisterUser)

		v1Route.Group(func(userAuthRoute chi.Router) {
			userAuthRoute.Use(mdw.AuthenticateToken)

			userAuthRoute.Post("/balance_topup", balanceDomainRestful.TopUp)
			userAuthRoute.Get("/balance_read", balanceRestful.GetBalanceByID)
			userAuthRoute.Post("/transfer", balanceDomainRestful.Transfer)

			userAuthRoute.Get("/top_users", balanceStatRestful.GetTopOutboundTransferRank)
			userAuthRoute.Get("/top_transactions_per_user", balanceStatRestful.GetTopTransfersByUsername)
		})
	})

	r.With(mdw.AuthenticateInternalToken).Get("/reset_stat", balanceStatRestful.ResetStat)

	if prepared != nil {
		prepared <- true
	}
	http.ListenAndServe(":7777", r)
}

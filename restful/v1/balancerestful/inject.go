package balancerestful

import (
	"simple-wallet/service/balanceservice"
	"sync"
)

var (
	balanceRestfulOnce sync.Once
	balanceRestfulVar  BalanceRestful
)

func InjectNewBalanceRestful() BalanceRestful {
	balanceRestfulOnce.Do(func() {
		balanceService := balanceservice.InjectNewBalanceService()
		balanceRestfulVar = &balanceRestful{
			balanceService: balanceService,
		}
	})

	return balanceRestfulVar
}

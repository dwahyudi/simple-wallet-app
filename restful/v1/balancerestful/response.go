package balancerestful

type (
	BalanceResponse struct {
		Balance float64 `json:"balance"`
	}
)

package balancerestful

import (
	"net/http"
	"simple-wallet/entity"
	rc "simple-wallet/restful/restfulcommon"
	"simple-wallet/service/balanceservice"
)

type (
	BalanceRestful interface {
		GetBalanceByID(w http.ResponseWriter, r *http.Request)
	}

	balanceRestful struct {
		balanceService balanceservice.BalanceService
	}
)

func (b *balanceRestful) GetBalanceByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	username := ctx.Value(entity.UsernameKey).(string)

	balance, err := b.balanceService.GetBalanceByID(ctx, username)
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	rc.SetJSONResponse(w, http.StatusOK, BalanceResponse{Balance: balance.Balance})
}

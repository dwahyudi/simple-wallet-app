package balancedomainrestful

import "simple-wallet/entity"

type (
	TransferForm struct {
		ToUsername string  `json:"to_username"`
		Amount     float64 `json:"amount"`
	}
)

func (t *TransferForm) Validate() error {
	if !(t.Amount >= 0) {
		return entity.ErrNonPositiveTransfer
	}

	if t.ToUsername == "" {
		return entity.ErrBlankUserName
	}

	return nil
}

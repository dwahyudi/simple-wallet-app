package balancedomainrestful

import "simple-wallet/entity"

type (
	TopupForm struct {
		Amount float64 `json:"amount"`
	}
)

func (r *TopupForm) Validate() error {
	if r.Amount < 1 {
		return entity.ErrNonPositiveTopup
	}

	if !(r.Amount < entity.MAX_TOPUP) {
		return entity.ErrTooBigTopup
	}

	return nil
}

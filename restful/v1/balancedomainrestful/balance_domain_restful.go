package balancedomainrestful

import (
	"encoding/json"
	"net/http"
	"simple-wallet/entity"
	rc "simple-wallet/restful/restfulcommon"
	"simple-wallet/service/balancedomainservice"
)

type (
	BalanceDomainRestful interface {
		Transfer(w http.ResponseWriter, r *http.Request)
		TopUp(w http.ResponseWriter, r *http.Request)
	}

	balanceDomainRestful struct {
		balanceDomainService balancedomainservice.BalanceDomainService
	}
)

func (b *balanceDomainRestful) Transfer(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var form TransferForm
	username := ctx.Value(entity.UsernameKey).(string)

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&form)
	if err != nil {
		rc.SetResponse(w, http.StatusBadRequest, "bad request")
		return
	}

	err = form.Validate()
	if err != nil {
		switch err {
		case entity.ErrNonPositiveTransfer:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Transfer should be more than 0")
		case entity.ErrBlankUserName:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Blank destination username")
		}
		return
	}

	err = b.balanceDomainService.Transfer(ctx, username, form.ToUsername, form.Amount)
	if err != nil {
		switch err {
		case entity.ErrNotEnoughBalanceForTransfer:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Insufficient balance")
		case entity.ErrTransferDestinationUsernameNotFound:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Destination user not found")
		}
		return
	}

	rc.SetResponse(w, http.StatusNoContent, "Transfer success")
}

func (b *balanceDomainRestful) TopUp(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	var form TopupForm

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&form)
	if err != nil {
		rc.SetResponse(w, http.StatusBadRequest, "bad request")
		return
	}

	err = form.Validate()
	if err != nil {
		switch err {
		case entity.ErrNonPositiveTopup:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Invalid topup amount")
		case entity.ErrTooBigTopup:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Topup amount too big")
		default:
			rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		}

		return
	}

	username := ctx.Value(entity.UsernameKey).(string)

	err = b.balanceDomainService.TopUp(ctx, username, form.Amount)
	if err != nil {
		switch err {
		case entity.ErrNonPositiveTopup:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Invalid topup amount")
		default:
			rc.SetResponse(w, http.StatusInternalServerError, "internal server error")
		}

		return
	}

	rc.SetResponse(w, http.StatusNoContent, "Topup successful")
}

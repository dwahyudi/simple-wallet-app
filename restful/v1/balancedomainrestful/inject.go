package balancedomainrestful

import (
	"simple-wallet/infra"
	"simple-wallet/service/balancedomainservice"
	"sync"
)

var (
	balanceDomainRestfulOnce sync.Once
	balanceDomainRestfulVar  BalanceDomainRestful
)

func InjectNewBalanceDomain(infra infra.Infra) BalanceDomainRestful {
	balanceDomainRestfulOnce.Do(func() {
		balanceDomainService := balancedomainservice.InjectNewBalanceDomainService()

		balanceDomainRestfulVar = &balanceDomainRestful{
			balanceDomainService: balanceDomainService,
		}
	})

	return balanceDomainRestfulVar
}

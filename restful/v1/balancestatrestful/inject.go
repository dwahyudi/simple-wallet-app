package balancestatrestful

import (
	"simple-wallet/service/balancestatservice"
	"sync"
)

var (
	balanceStatRestfulOnce sync.Once
	balanceStatRestfulVar  BalanceStatRestful
)

func InjectNewBalanceStatRestful() BalanceStatRestful {
	balanceStatRestfulOnce.Do(func() {
		balanceStatService := balancestatservice.InjectNewBalanceStatService()
		balanceStatRestfulVar = &balanceStatRestful{
			balanceStatService: balanceStatService,
		}
	})

	return balanceStatRestfulVar
}

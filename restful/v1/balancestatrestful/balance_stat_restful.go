package balancestatrestful

import (
	"net/http"
	"simple-wallet/entity"
	rc "simple-wallet/restful/restfulcommon"
	"simple-wallet/service/balancestatservice"
)

type (
	BalanceStatRestful interface {
		GetTopOutboundTransferRank(w http.ResponseWriter, r *http.Request)
		GetTopTransfersByUsername(w http.ResponseWriter, r *http.Request)

		ResetStat(w http.ResponseWriter, r *http.Request)
	}

	balanceStatRestful struct {
		balanceStatService balancestatservice.BalanceStatService
	}
)

func (b *balanceStatRestful) GetTopOutboundTransferRank(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	topRanks, err := b.balanceStatService.GetTopOutboundTransferRank(ctx)
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	rc.SetJSONResponse(w, http.StatusOK, topRanks)
}

func (b *balanceStatRestful) GetTopTransfersByUsername(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	username := ctx.Value(entity.UsernameKey).(string)

	topRanks, err := b.balanceStatService.GetTopTransfersByUsername(ctx, username)
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	rc.SetJSONResponse(w, http.StatusOK, topRanks)
}

func (b *balanceStatRestful) ResetStat(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	err := b.balanceStatService.ResetStat(ctx)
	if err != nil {
		rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		return
	}

	rc.SetResponse(w, http.StatusOK, "ok")
}

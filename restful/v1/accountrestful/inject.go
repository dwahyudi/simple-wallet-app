package accountrestful

import (
	"simple-wallet/infra"
	"simple-wallet/service/accountservice"
	"simple-wallet/service/balanceservice"
	"sync"
)

var (
	accountRestfulOnce sync.Once
	accountRestfulVar  AccountRestful
)

func InjectNewRestfulVar(
	infra infra.Infra,
	balanceService balanceservice.BalanceService,
	accountService accountservice.AccountService,
) AccountRestful {
	accountRestfulOnce.Do(func() {
		accountRestfulVar = &accountRestful{
			balanceService: balanceService,
			accountService: accountService,
		}
	})

	return accountRestfulVar
}

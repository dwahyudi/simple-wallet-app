package accountrestful

import (
	"encoding/json"
	"net/http"
	"simple-wallet/entity"
	rc "simple-wallet/restful/restfulcommon"
	"simple-wallet/service/accountservice"
	"simple-wallet/service/balanceservice"
)

type (
	AccountRestful interface {
		RegisterUser(w http.ResponseWriter, r *http.Request)
	}

	accountRestful struct {
		balanceService balanceservice.BalanceService
		accountService accountservice.AccountService
	}
)

func (b *accountRestful) RegisterUser(w http.ResponseWriter, r *http.Request) {
	var form RegisterForm
	ctx := r.Context()

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&form)
	if err != nil {
		rc.SetResponse(w, http.StatusBadRequest, "bad request")
		return
	}

	err = form.Validate()
	if err != nil {
		switch err {
		case entity.ErrBlankUserName:
			rc.SetResponse(w, http.StatusUnprocessableEntity, "Cannot register blank username")
		default:
			rc.SetResponse(w, http.StatusUnprocessableEntity, err.Error())
		}

		return
	}

	token, err := b.accountService.Register(ctx, form.Username)
	if err != nil {
		rc.SetResponse(w, http.StatusInternalServerError, "internal server error")
		return
	}

	err = b.balanceService.CreateNewBalance(ctx, form.Username)
	if err != nil {
		switch err {
		case entity.ErrUserAlreadyExist:
			rc.SetResponse(w, http.StatusConflict, "Username already exists")
		default:
			rc.SetResponse(w, http.StatusInternalServerError, "internal server error")
		}

		return
	}

	rc.SetResponse(w, http.StatusCreated, token)
}

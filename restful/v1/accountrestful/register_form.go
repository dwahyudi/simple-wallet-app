package accountrestful

import "simple-wallet/entity"

type (
	RegisterForm struct {
		Username string `json:"username"`
	}
)

func (r *RegisterForm) Validate() error {
	if r.Username == "" {
		return entity.ErrBlankUserName
	}

	return nil
}

package main

import (
	"os"
	"simple-wallet/external"
	"simple-wallet/infra"
	"simple-wallet/restful"
)

func main() {
	args := os.Args

	switch args[0] {
	case "api":
		runRESTfulAPI()
	default:
		runRESTfulAPI()
	}
}

func runRESTfulAPI() {
	infraInject := infra.NewInfra("config/app.toml", external.InjectNewExternalService("config/app.toml", nil))

	restful.InjectNewServer(infraInject).Run(nil)
}

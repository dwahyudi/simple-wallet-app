##  (2022-05-27)




## 0.1.0 (2022-05-27)

* build: add makefile, add e2e, api and build 07cd9a9
* test: add e2e TestV1RegisterUserBlankUsername a325d70
* test: add TestV1Topup 396934c
* test: e2e add TestV1Topup_NonPositiveAmount f997f45
* test: e2e add TestV1Topup_TooBigAmount e932ee3
* test: e2e add TestV1TopUsers for randomness of transfers order of users f3afc6c
* test: e2e add TestV1Transfer 2f0c396
* test: e2e add TestV1Transfer_BlankDestination 1a14e74
* test: e2e add TestV1Transfer_BlankDestinationUsername df0fe29
* test: e2e add TestV1Transfer_InsufficientBalance 43262d0
* test: e2e add TestV1Transfer_NonPositiveTransfer aa3f248
* test: e2e add top users test b8d2335
* test: e2e fix inverted assertions 37fba49
* test: e2e fix TestV1Topup_TooBigAmount 3503e47
* test: e2e fix TestV1Topup_TooBigAmount 9b9d2e5
* test: e2e fix TestV1TopUsers_MoreThan10 destination of transfers b4d8d33
* test: e2e for top transfer rank dcf372e
* test: e2e rename to TestV1Transfer_DestinationUsernameNotFound cd506ae
* test: kickstart e2e test, start with test register user af91bbf
* test: rename to TestV1RegisterUser_BlankUsername and use log fatal instead of panic f96f4f5
* test: TestV1RegisterUser add when duplicate username case 6180b35
* feat: add /balance_read handler 93e2fca
* feat: add /balance_topup route 7ddc59e
* feat: add /transfer endpoint ea1e9fa
* feat: add accountservice and its injector, add Register method aef5aaa
* feat: add AuthenticateInternalToken in mdw 3fb5944
* feat: add balancedomainrepo injector 7b99bf4
* feat: add balancedomainrepo, add Transfer method c902117
* feat: add balancedomainservice and its injector 1870a72
* feat: add balancerepo memory implementation injection b4b4a69
* feat: add balancerepo package, start with CreateNewBalance method, with memory implementation ef50e6c
* feat: add balanceservice, GetBalanceByID method 98aabc2
* feat: add balancestatrestful, connect with previous service layer 2ecf240
* feat: add balancestatservice for getting top outbound transfers and resetting the stat 10a364a
* feat: add event package, general observer pattern, for now add for transfer notification f5c4190
* feat: add external package and its injector 72e4315
* feat: add GetTopTransfersByUsername in balancestatrestfulservice 82689c9
* feat: add infra package and its injector bcf0c64
* feat: add jwt external dependencies 247cce0
* feat: add JWTValidateLogin method, for validating jwt token against a secret from env 90a9458
* feat: add logic to save and read top transfer from and to rank tree 909e0dc
* feat: add max limit of topup amount d9a9309
* feat: add mdw package, this is for custom restful middleware 3bf91f6
* feat: add restful routes for top users, also add for resetting the data (for e2e) 54f174a
* feat: add restful routing, connect with the existing service(s) 3215ded
* feat: add restful/v1/accountrestful package, add RegisterUser handler 19be11e
* feat: add restfulcommon package, add some common methods for building http responses 9121c8b
* feat: add top_transactions_per_user route connect with previous service a29ebf3
* feat: add TopUp restful handler 06d7097
* feat: balancedomainrepo add Topup method 559cc99
* feat: balancedomainservice add Transfer method cbc3e5d
* feat: balancerepo add GetBalanceByID 50b7da7
* feat: balancerepo add UpdateBalanceByID 8821bda
* feat: balanceservice add GetBalanceByID 49aeaa9
* feat: balanceservice add injection 788e47b
* feat: go mod init project 5002bb6
* feat: now add transfer notification, using event package 5575972
* feat: repo add balancestatrepo, for now it's for collecting top outbound transfers ab0207c
* feat: update main.go to call the restful server run/launcher 1c6217a
* refactor: InjectNewExternalService to receive config path 67e4799
* refactor: naming for update outbound transfer total rank methods c03e97c
* fix: add some error types d0deea3
* fix: balance UpdateBalanceByID locking a97b217
* fix: balancedomainrepo Transfer return errors ca0cf21
* fix: balancesData use pointer to balance data of entity.Balance e386439
* fix: error message when registering user f2ed98b
* fix: error message when validating topup fc6d297
* fix: initialization of mutex when creating balance d48978f
* fix: injector for restful, infra add config 0aeb8e9
* fix: missing Account entity used by jwt service 267426b
* fix: restful run signature, for e2e preparation a9403f8
* fix: UpdateBalanceByID logic and locking d90a7e7
* fix: wrong package name of balancedomainrepo 3f9bdae
* chore: add .gitignore 1ce573d




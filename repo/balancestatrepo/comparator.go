package balancestatrepo

import (
	"math"
	"simple-wallet/entity"
)

// comparator for the tree data storage of rank
func OutboundTransferTotalCmp(a, b interface{}) int {
	var (
		aAsserted = a.(entity.OutboundTransferTotal)
		bAsserted = b.(entity.OutboundTransferTotal)
		aAmount   = aAsserted.TransactedValue
		bAmount   = bAsserted.TransactedValue
		aUsername = aAsserted.UserName
		bUsername = bAsserted.UserName
	)

	// handle when same key/amount
	if aUsername == bUsername {
		return 0
	} else {
		switch {
		case aAmount < bAmount:
			return 1
		default:
			return -1
		}
	}
}

func TopTransferCmp(a, b interface{}) int {
	var (
		aAsserted  = a.(entity.TransferDestination)
		bAsserted  = b.(entity.TransferDestination)
		aAmount    = aAsserted.Amount
		bAmount    = bAsserted.Amount
		absAAmount = math.Abs(aAmount)
		absBAmount = math.Abs(bAmount)
	)

	if absAAmount < absBAmount {
		return 1
	} else {
		return -1
	}
}

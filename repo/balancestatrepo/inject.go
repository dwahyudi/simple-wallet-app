package balancestatrepo

import (
	"simple-wallet/entity"
	"sync"

	"github.com/emirpasic/gods/trees/avltree"
)

var (
	balanceStatRepoOnce sync.Once
	balanceStatRepo     BalanceStatRepo
)

func InjectNewBalanceRepo() BalanceStatRepo {
	balanceStatRepoOnce.Do(func() {
		balanceStatRepo = &balanceStatMemory{
			outboundTransferTotals:         make(map[string]*entity.OutboundTransferTotalWithLock),
			outboundTransferTotalsRank:     avltree.NewWith(OutboundTransferTotalCmp),
			outboundTransferTotalsRankLock: &sync.Mutex{},

			topTransfers: make(map[string]*avltree.Tree),
		}
	})

	return balanceStatRepo
}

package balancestatrepo

import (
	"context"
	"simple-wallet/entity"
	"sync"

	"github.com/emirpasic/gods/trees/avltree"
)

func (b *balanceStatMemory) Update(ctx context.Context, transferEvent entity.TransferEvent) error {
	err := b.UpdateOutboundTransferTotalByUsernameToRank(ctx, transferEvent)
	if err != nil {
		return err
	}

	err = b.UpdateTopTransferByUsernameToRank(ctx, transferEvent)
	if err != nil {
		return err
	}

	return nil
}

func (b *balanceStatMemory) UpdateTopTransferByUsernameToRank(ctx context.Context, transferEvent entity.TransferEvent) error {
	var (
		sourceUsername      = transferEvent.SourceUsername
		destinationUsername = transferEvent.DestinationUsername
		transferAmount      = transferEvent.Amount
	)

	// update source rank
	sourceUserTopTransferRank, err := b.GetOrCreateTopTransferRankByUsername(ctx, sourceUsername)
	if err != nil {
		return err
	}

	sourceUserTopTransferRank.Put(entity.TransferDestination{
		Amount: -transferAmount, DestinationUsername: destinationUsername,
	}, sourceUsername)

	// update destination rank
	destinationUserTopTransferRank, err := b.GetOrCreateTopTransferRankByUsername(ctx, destinationUsername)
	if err != nil {
		return err
	}

	destinationUserTopTransferRank.Put(entity.TransferDestination{
		Amount: transferAmount, DestinationUsername: sourceUsername,
	}, destinationUsername)

	return nil
}

func (b *balanceStatMemory) GetOrCreateTopTransferRankByUsername(ctx context.Context, sourceUsername string) (*avltree.Tree, error) {
	foundExistingUserTopTransferRank, found := b.topTransfers[sourceUsername]
	if !found {
		newUserTopTransferRank := avltree.NewWith(TopTransferCmp)

		b.topTransfers[sourceUsername] = newUserTopTransferRank
		return newUserTopTransferRank, nil
	}

	return foundExistingUserTopTransferRank, nil
}

func (b *balanceStatMemory) UpdateOutboundTransferTotalByUsernameToRank(ctx context.Context, transferEvent entity.TransferEvent) error {
	b.outboundTransferTotalsRankLock.Lock()
	defer b.outboundTransferTotalsRankLock.Unlock()

	sourceUsername := transferEvent.SourceUsername

	previousAmountData, err := b.GetOrCreateOutboundTransferTotalsByUsername(ctx, sourceUsername)
	if err != nil {
		return err
	}
	previousAmount := previousAmountData.TransactedValue

	newAmount, err := b.UpdateOutboundTransferTotalByUsername(ctx, sourceUsername, transferEvent.Amount)
	if err != nil {
		return err
	}

	b.outboundTransferTotalsRank.Remove(entity.OutboundTransferTotal{UserName: sourceUsername, TransactedValue: previousAmount})
	b.outboundTransferTotalsRank.Put(entity.OutboundTransferTotal{UserName: sourceUsername, TransactedValue: newAmount}, sourceUsername)

	return nil
}

func (b *balanceStatMemory) UpdateOutboundTransferTotalByUsername(ctx context.Context, username string, amount float64) (float64, error) {
	// acquire lock
	currentOutboundTotalTransferForLock, err := b.GetOrCreateOutboundTransferTotalsByUsername(ctx, username)
	if err != nil {
		return 0, err
	}

	currentOutboundTotalTransferForLock.Mutex.Lock()
	defer currentOutboundTotalTransferForLock.Mutex.Unlock()

	// acquire total amount, make sure mutex is under lock
	currentOutboundTotalTransfer, err := b.GetOrCreateOutboundTransferTotalsByUsername(ctx, username)
	if err != nil {
		return 0, err
	}

	// adjust new total amount
	newTotalAmount := currentOutboundTotalTransfer.TransactedValue + amount

	// save the new total amount
	b.outboundTransferTotals[username].TransactedValue = newTotalAmount

	return newTotalAmount, nil
}

func (b *balanceStatMemory) GetOrCreateOutboundTransferTotalsByUsername(
	ctx context.Context, username string,
) (*entity.OutboundTransferTotalWithLock, error) {
	currentOutboundTotalTransfer, found := b.outboundTransferTotals[username]
	if !found {
		newOutboundTransferTotal := &entity.OutboundTransferTotalWithLock{
			Mutex: &sync.Mutex{},
			OutboundTransferTotal: entity.OutboundTransferTotal{
				UserName: username, TransactedValue: 0,
			},
		}

		b.outboundTransferTotals[username] = newOutboundTransferTotal
		return newOutboundTransferTotal, nil
	}

	return currentOutboundTotalTransfer, nil
}

func (b *balanceStatMemory) GetTopOutboundTransferRank(ctx context.Context) ([]entity.OutboundTransferTotal, error) {
	topRanks := make([]entity.OutboundTransferTotal, 0)

	// get data from the left-most of the tree (one with the highest outbound transfer)
	topRank := b.outboundTransferTotalsRank.Left()

	// completely empty rank
	if topRank == nil {
		return topRanks, nil
	}

	topRanks = append(topRanks, topRank.Key.(entity.OutboundTransferTotal))

	var (
		rankSize     = b.outboundTransferTotalsRank.Size()
		currentCount = 0
		currentRank  = topRank
	)

	for i := 0; i < rankSize-1; i++ {
		currentRank = currentRank.Next()

		currentCount += 1
		// means that ranks count is more than OUTBOUND_TOP_RANK_COUNT
		if currentCount >= entity.OUTBOUND_TOP_RANK_COUNT {
			return topRanks, nil
		}

		topRanks = append(topRanks, currentRank.Key.(entity.OutboundTransferTotal))
	}

	return topRanks, nil
}

func (b *balanceStatMemory) GetTopTransfersByUsername(ctx context.Context, username string) ([]entity.TransferDestination, error) {
	topRanks := make([]entity.TransferDestination, 0)

	topTransfer, err := b.GetOrCreateTopTransferRankByUsername(ctx, username)
	if err != nil {
		return nil, err
	}

	topRank := topTransfer.Left()

	// completely empty rank, user hasn't made any transfer
	if topRank == nil {
		return topRanks, nil
	}

	topRanks = append(topRanks, topRank.Key.(entity.TransferDestination))

	var (
		rankSize     = topTransfer.Size()
		currentCount = 0
		currentRank  = topRank
	)

	for i := 0; i < rankSize-1; i++ {
		currentRank = currentRank.Next()

		currentCount += 1
		// means that ranks count is more than OUTBOUND_TOP_RANK_COUNT
		if currentCount >= entity.OUTBOUND_TOP_RANK_COUNT {
			return topRanks, nil
		}

		topRanks = append(topRanks, currentRank.Key.(entity.TransferDestination))
	}

	return topRanks, nil
}

func (b *balanceStatMemory) ResetStat(ctx context.Context) error {
	b.outboundTransferTotals = make(map[string]*entity.OutboundTransferTotalWithLock)
	b.outboundTransferTotalsRank = avltree.NewWith(OutboundTransferTotalCmp)

	return nil
}

package balancestatrepo

import (
	"context"
	"simple-wallet/entity"
	"sync"

	"github.com/emirpasic/gods/trees/avltree"
)

type (
	BalanceStatRepo interface {
		Update(ctx context.Context, transferEvent entity.TransferEvent) error

		GetTopOutboundTransferRank(ctx context.Context) ([]entity.OutboundTransferTotal, error)
		GetTopTransfersByUsername(ctx context.Context, username string) ([]entity.TransferDestination, error)

		ResetStat(ctx context.Context) error
	}

	balanceStatMemory struct {
		outboundTransferTotals         map[string]*entity.OutboundTransferTotalWithLock
		outboundTransferTotalsRankLock *sync.Mutex
		outboundTransferTotalsRank     *avltree.Tree

		topTransfers map[string]*avltree.Tree
	}
)

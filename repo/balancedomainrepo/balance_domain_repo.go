package balancedomainrepo

import (
	"context"
	"errors"
	"fmt"
	"simple-wallet/entity"
	"simple-wallet/repo/balancerepo"
)

type (
	BalanceDomainRepo interface {
		TopUp(ctx context.Context, username string, amount float64) error
		Transfer(ctx context.Context, sourceUsername string, destinationUsername string, amount float64) error
	}

	balanceDomainRepo struct {
		balanceRepo balancerepo.BalanceRepo
	}
)

func (b *balanceDomainRepo) TopUp(ctx context.Context, username string, amount float64) error {
	err := b.balanceRepo.UpdateBalanceByID(ctx, username, amount)
	if err != nil {
		return err
	}

	return nil
}

func (b *balanceDomainRepo) Transfer(ctx context.Context, sourceUsername string, destinationUsername string, amount float64) error {
	_, err := b.balanceRepo.GetBalanceByID(ctx, sourceUsername)
	if err != nil {
		return fmt.Errorf("%w: %s", err, sourceUsername)
	}

	_, err = b.balanceRepo.GetBalanceByID(ctx, destinationUsername)
	if err != nil {
		return entity.ErrTransferDestinationUsernameNotFound
	}

	err = b.balanceRepo.UpdateBalanceByID(ctx, sourceUsername, -amount)
	if err != nil {
		if errors.Is(err, entity.ErrNegativeNewBalance) {
			return entity.ErrNotEnoughBalanceForTransfer
		}
		return err
	}

	err = b.balanceRepo.UpdateBalanceByID(ctx, destinationUsername, amount)
	if err != nil {
		return err
	}

	return nil
}

package balancedomainrepo

import (
	"simple-wallet/repo/balancerepo"
	"sync"
)

var (
	balanceDomainRepoOnce sync.Once
	balanceDomainMemory   BalanceDomainRepo
)

func InjectNewBalanceDomainRepoUseMemory() BalanceDomainRepo {
	balanceDomainRepoOnce.Do(func() {
		balanceRepo := balancerepo.InjectNewBalanceRepoUseMemory()

		balanceDomainMemory = &balanceDomainRepo{
			balanceRepo: balanceRepo,
		}
	})

	return balanceDomainMemory
}

package balancerepo

import (
	"context"
	"simple-wallet/entity"
)

type (
	BalanceRepo interface {
		CreateNewBalance(ctx context.Context, username string) error
		GetBalanceByID(ctx context.Context, username string) (*entity.Balance, error)
		UpdateBalanceByID(ctx context.Context, username string, amount float64) error
	}

	balanceMemory struct {
		balancesData map[string]*entity.Balance
	}
)

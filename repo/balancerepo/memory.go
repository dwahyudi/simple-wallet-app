package balancerepo

import (
	"context"
	"simple-wallet/entity"
	"sync"
)

func (b *balanceMemory) CreateNewBalance(ctx context.Context, username string) error {
	isExist, err := b.IsExist(ctx, username)
	if err != nil {
		return err
	}
	if isExist {
		return entity.ErrUserAlreadyExist
	}

	b.balancesData[username] = &entity.Balance{UserName: username, Balance: 0, Mutex: &sync.Mutex{}}

	return nil
}

func (b *balanceMemory) GetBalanceByID(ctx context.Context, username string) (*entity.Balance, error) {
	balance, found := b.balancesData[username]
	if !found {
		return &entity.Balance{}, entity.ErrUserNotFound
	}

	return balance, nil
}

func (b *balanceMemory) IsExist(ctx context.Context, username string) (bool, error) {
	_, found := b.balancesData[username]
	return found, nil
}

func (b *balanceMemory) UpdateBalanceByID(ctx context.Context, username string, amount float64) error {
	// acquire lock
	currentBalanceForLock, err := b.GetBalanceByID(ctx, username)
	if err != nil {
		return err
	}

	currentBalanceForLock.Mutex.Lock()
	defer currentBalanceForLock.Mutex.Unlock()

	// acquire balance, make sure mutex is under lock
	currentBalance, err := b.GetBalanceByID(ctx, username)
	if err != nil {
		return err
	}

	// adjust new balance amount
	newBalance := currentBalance.Balance + amount
	if newBalance < 0 {
		return entity.ErrNegativeNewBalance
	}

	// save the new balance
	currentBalance.Balance = newBalance

	return nil
}

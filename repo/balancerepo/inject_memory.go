package balancerepo

import (
	"simple-wallet/entity"
	"sync"
)

var (
	balanceRepoMemoryOnce sync.Once
	balanceRepoMemory     BalanceRepo
)

func InjectNewBalanceRepoUseMemory() BalanceRepo {
	balanceRepoMemoryOnce.Do(func() {
		balanceRepoMemory = &balanceMemory{
			balancesData: make(map[string]*entity.Balance),
		}
	})

	return balanceRepoMemory
}

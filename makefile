.PHONY: e2e api build

e2e:
	@go1.18 test ./e2e -v -coverpkg=./... -shuffle=on -cover -count=1
api:
	@go1.18 run main.go api
build:
	@go1.18 build
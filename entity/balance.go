package entity

import "sync"

type (
	Balance struct {
		Mutex *sync.Mutex

		UserName string
		Balance  float64
	}

	TransferEvent struct {
		SourceUsername      string
		DestinationUsername string
		Amount              float64
	}

	OutboundTransferTotalWithLock struct {
		Mutex *sync.Mutex

		OutboundTransferTotal
	}

	OutboundTransferTotal struct {
		UserName        string  `json:"username"`
		TransactedValue float64 `json:"transacted_value"`
	}

	TransferDestination struct {
		DestinationUsername string  `json:"username"`
		Amount              float64 `json:"amount"`
	}
)

const (
	MAX_TOPUP                = 10000000
	OUTBOUND_TOP_RANK_COUNT  = 10
	USER_TRANSFER_RANK_COUNT = 10
)

package entity

import "fmt"

var (
	ErrUserNotFound                        = fmt.Errorf("user not found")
	ErrUserAlreadyExist                    = fmt.Errorf("user already exist")
	ErrBlankUserName                       = fmt.Errorf("blank username")
	ErrNonPositiveTopup                    = fmt.Errorf("non positive topup amount")
	ErrTooBigTopup                         = fmt.Errorf("topup amount to big")
	ErrNegativeNewBalance                  = fmt.Errorf("negative new balance")
	ErrNotEnoughBalanceForTransfer         = fmt.Errorf("not enough balance for transfer")
	ErrNonPositiveTransfer                 = fmt.Errorf("non positive transfer amount")
	ErrTransferDestinationUsernameNotFound = fmt.Errorf("destination user not found")
)

package entity

type (
	Account struct {
		Username string `json:"username"`
	}
)

const (
	UsernameKey = "username-key"
)

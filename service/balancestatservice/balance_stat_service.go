package balancestatservice

import (
	"context"
	"simple-wallet/entity"
	"simple-wallet/repo/balancestatrepo"
)

type (
	BalanceStatService interface {
		GetTopOutboundTransferRank(ctx context.Context) ([]entity.OutboundTransferTotal, error)
		GetTopTransfersByUsername(
			ctx context.Context, username string,
		) ([]entity.TransferDestination, error)

		ResetStat(ctx context.Context) error
	}

	balanceStatService struct {
		balanceStatRepo balancestatrepo.BalanceStatRepo
	}
)

func (b *balanceStatService) GetTopOutboundTransferRank(ctx context.Context) ([]entity.OutboundTransferTotal, error) {
	topRanks, err := b.balanceStatRepo.GetTopOutboundTransferRank(ctx)
	if err != nil {
		return nil, err
	}

	return topRanks, nil
}

func (b *balanceStatService) GetTopTransfersByUsername(
	ctx context.Context, username string,
) ([]entity.TransferDestination, error) {
	topRanks, err := b.balanceStatRepo.GetTopTransfersByUsername(ctx, username)
	if err != nil {
		return nil, err
	}

	return topRanks, nil
}

func (b *balanceStatService) ResetStat(ctx context.Context) error {
	err := b.balanceStatRepo.ResetStat(ctx)
	if err != nil {
		return err
	}

	return nil
}

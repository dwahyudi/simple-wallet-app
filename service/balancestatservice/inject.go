package balancestatservice

import (
	"simple-wallet/repo/balancestatrepo"
	"sync"
)

var (
	BalanceStatServiceOnce sync.Once
	balanceStatSvc         BalanceStatService
)

func InjectNewBalanceStatService() BalanceStatService {
	BalanceStatServiceOnce.Do(func() {
		balanceStatRepo := balancestatrepo.InjectNewBalanceRepo()

		balanceStatSvc = &balanceStatService{
			balanceStatRepo: balanceStatRepo,
		}
	})

	return balanceStatSvc
}

package accountservice

import (
	"context"
	"simple-wallet/entity"
	"simple-wallet/external/extcommon"
)

type (
	AccountService interface {
		Register(ctx context.Context, username string) (string, error)
	}

	accountService struct {
		extCommonSvc extcommon.CommonService
	}
)

func (a *accountService) Register(ctx context.Context, username string) (string, error) {
	token, err := a.extCommonSvc.JWTLoginBuild(entity.Account{Username: username})
	if err != nil {
		return "", err
	}

	return token, nil
}

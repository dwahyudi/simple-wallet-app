package accountservice

import (
	"simple-wallet/infra"
	"sync"
)

var (
	accountServiceOnce sync.Once
	accountSvc         AccountService
)

func InjectNewAccountService(infra infra.Infra) AccountService {
	accountServiceOnce.Do(func() {
		accountSvc = &accountService{extCommonSvc: infra.External().CommonSvc()}
	})

	return accountSvc
}

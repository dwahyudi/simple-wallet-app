package balanceservice

import (
	"simple-wallet/repo/balancerepo"
	"sync"
)

var (
	balanceServiceOnce sync.Once
	balanceSvc         BalanceService
)

func InjectNewBalanceService() BalanceService {
	balanceServiceOnce.Do(func() {
		balanceRepo := balancerepo.InjectNewBalanceRepoUseMemory()

		balanceSvc = &balanceService{balancerepo: balanceRepo}
	})

	return balanceSvc
}

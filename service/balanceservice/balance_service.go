package balanceservice

import (
	"context"
	"simple-wallet/entity"
	"simple-wallet/repo/balancerepo"
)

type (
	BalanceService interface {
		CreateNewBalance(ctx context.Context, username string) error
		GetBalanceByID(ctx context.Context, username string) (entity.Balance, error)
	}

	balanceService struct {
		balancerepo balancerepo.BalanceRepo
	}
)

func (b *balanceService) CreateNewBalance(ctx context.Context, username string) error {
	err := b.balancerepo.CreateNewBalance(ctx, username)
	if err != nil {
		return err
	}

	return nil
}

func (b *balanceService) GetBalanceByID(ctx context.Context, username string) (entity.Balance, error) {
	balance, err := b.balancerepo.GetBalanceByID(ctx, username)
	if err != nil {
		return entity.Balance{}, err
	}

	return *balance, nil
}

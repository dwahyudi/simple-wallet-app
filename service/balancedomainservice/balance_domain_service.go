package balancedomainservice

import (
	"context"
	"simple-wallet/entity"
	"simple-wallet/event"
	"simple-wallet/repo/balancedomainrepo"
)

type (
	BalanceDomainService interface {
		TopUp(ctx context.Context, username string, amount float64) error
		Transfer(ctx context.Context, sourceUsername string, destinationUsername string, amount float64) error
	}

	balanceDomainService struct {
		balanceDomainRepo balancedomainrepo.BalanceDomainRepo
		balanceEvent      event.BalanceEvent
	}
)

func (b *balanceDomainService) TopUp(ctx context.Context, username string, amount float64) error {
	err := b.balanceDomainRepo.TopUp(ctx, username, amount)
	if err != nil {
		return err
	}

	return nil
}

func (b *balanceDomainService) Transfer(ctx context.Context, sourceUsername string, destinationUsername string, amount float64) error {
	err := b.balanceDomainRepo.Transfer(ctx, sourceUsername, destinationUsername, amount)
	if err != nil {
		return err
	}

	b.balanceEvent.TransferNotify(ctx, entity.TransferEvent{
		SourceUsername:      sourceUsername,
		DestinationUsername: destinationUsername,
		Amount:              amount,
	})

	return nil
}

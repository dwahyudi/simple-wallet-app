package balancedomainservice

import (
	"simple-wallet/event"
	"simple-wallet/repo/balancedomainrepo"
	"sync"
)

var (
	balanceDomainServiceOnce sync.Once
	balanceDomainSvc         BalanceDomainService
)

func InjectNewBalanceDomainService() BalanceDomainService {
	balanceDomainServiceOnce.Do(func() {
		balanceDomainRepo := balancedomainrepo.InjectNewBalanceDomainRepoUseMemory()
		balanceEvent := event.InjectNewBalanceService()
		balanceDomainSvc = &balanceDomainService{
			balanceDomainRepo: balanceDomainRepo,
			balanceEvent:      balanceEvent,
		}
	})

	return balanceDomainSvc
}

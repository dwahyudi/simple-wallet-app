package infra

import (
	"simple-wallet/external"
	"sync"
)

type (
	Infra interface {
		External() external.External
		Config() Config
	}

	DB struct {
	}

	infra struct {
		config          Config
		externalService external.External
	}

	Config struct {
		Internal InternalSecret `toml:"internal"`
	}

	InternalSecret struct {
		InternalSecret string `toml:"internal_secret"`
	}
)

var (
	externalSvcOnce sync.Once
	externalSvc     external.External
)

func (i *infra) External() external.External {
	externalSvcOnce.Do(func() {
		externalSvc = i.externalService
	})

	return externalSvc
}

var (
	configOnce sync.Once
	configSvc  Config
)

func (i *infra) Config() Config {
	configOnce.Do(func() {
		configSvc = i.config
	})

	return configSvc
}

package infra

import (
	"log"
	"os"
	"simple-wallet/external"
	"sync"

	"github.com/BurntSushi/toml"
)

var (
	infraOnce sync.Once
	infraVar  Infra
)

func NewInfra(
	configPath string,
	ext external.External,
) Infra {
	var cfg Config

	cfgFile, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatal(err)
	}

	_, err = toml.Decode(string(cfgFile), &cfg)
	if err != nil {
		log.Fatal(err)
	}

	return &infra{
		config:          cfg,
		externalService: ext,
	}
}

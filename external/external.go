package external

import (
	"simple-wallet/external/extcommon"
	"sync"
)

type (
	External interface {
		CommonSvc() extcommon.CommonService
	}

	config struct {
		JWTConfig JWTConfig `toml:"jwtconfig"`
	}

	JWTConfig struct {
		LoginSecret string `toml:"login_secret"`
	}

	external struct {
		commonSvc extcommon.CommonService
	}
)

var (
	commonSvcOnce sync.Once
	commonSvc     extcommon.CommonService
)

func (e *external) CommonSvc() extcommon.CommonService {
	commonSvcOnce.Do(func() {
		commonSvc = e.commonSvc
	})

	return commonSvc
}

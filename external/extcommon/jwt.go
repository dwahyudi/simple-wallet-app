package extcommon

import (
	"fmt"
	"simple-wallet/entity"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

type (
	JWTService interface {
		JWTLoginBuild(account entity.Account) (string, error)
		JWTValidateLogin(tokenString string) (string, error)
	}

	AccountClaim struct {
		Username string `json:"username"`
		jwt.RegisteredClaims
	}

	LoginClaim struct {
		Username string `json:"username"`
	}
)

func (rcs *RealCommonService) JWTLoginBuild(account entity.Account) (string, error) {
	claims := AccountClaim{
		account.Username,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(96 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			NotBefore: jwt.NewNumericDate(time.Now()),
			Issuer:    "simple-wallet",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	secret := []byte(rcs.LoginSecret)
	signedToken, err := token.SignedString(secret)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func (rcs *RealCommonService) JWTValidateLogin(tokenString string) (string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(rcs.LoginSecret), nil
	})
	if err != nil {
		return "", err
	}

	var username string

	if token != nil {
		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			var userIdClaim = claims["username"]
			fetchedUsername := userIdClaim.(string)

			username = string(fetchedUsername)
		}
	}

	return username, nil
}

package extcommon

type (
	CommonService interface {
		JWTService
	}

	RealCommonService struct {
		LoginSecret string
	}
)

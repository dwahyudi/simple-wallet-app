package extcommon

import "sync"

var (
	ExtCommonOnce sync.Once
	extCommon     CommonService
)

func InjectNewExtCommon(
	loginSecret string,
) CommonService {
	ExtCommonOnce.Do(func() {
		extCommon = &RealCommonService{
			LoginSecret: loginSecret,
		}
	})

	return extCommon
}

package external

import (
	"log"
	"os"
	"simple-wallet/external/extcommon"
	"sync"

	"github.com/BurntSushi/toml"
)

var (
	externalOnce sync.Once
	externalSvc  External
)

func InjectNewExternalService(
	configPath string,
	commonSvc *extcommon.CommonService,
) External {
	var cfg config
	var decidedCommonSvc extcommon.CommonService

	cfgFile, err := os.ReadFile(configPath)
	if err != nil {
		log.Fatal(err)
	}

	_, err = toml.Decode(string(cfgFile), &cfg)
	if err != nil {
		log.Fatal(err)
	}

	if commonSvc == nil {
		decidedCommonSvc = extcommon.InjectNewExtCommon(cfg.JWTConfig.LoginSecret)
	}

	return &external{commonSvc: decidedCommonSvc}
}
